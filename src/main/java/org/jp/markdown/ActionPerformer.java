package org.jp.markdown;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.VisualPosition;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ActionPerformer {
    public static void perform(@NotNull AnActionEvent event, String actionName) {

        //        //获取当前在操作的工程上下文
//        Project project = e.getData(PlatformDataKeys.PROJECT);
//
//        //获取当前操作的类文件
//        PsiFile psiFile = e.getData(CommonDataKeys.PSI_FILE);
//        //获取当前类文件的路径
//        String classPath = psiFile.getVirtualFile().getPath();
//        String title = "Hello World!";
//
//        //显示对话框
//        Messages.showMessageDialog(project, classPath, title, Messages.getInformationIcon());
        // Get all the required data from data keys
        Editor editor = event.getRequiredData(CommonDataKeys.EDITOR);
        Project project = event.getRequiredData(CommonDataKeys.PROJECT);
        Document doc = editor.getDocument();

        // Work off of the primary caret to get the selection info
        //caret:光标
        Caret primaryCaret = editor.getCaretModel().getPrimaryCaret();
        VisualPosition vp = primaryCaret.getVisualPosition();
        if (vp.column == 0) {
            return;
        }
        //LogicalPosition lp = primaryCaret.getLogicalPosition();
//        int start = primaryCaret.getSelectionStart();
        int thisStart = doc.getLineStartOffset(vp.line);
        int thisEnd = doc.getLineEndOffset(vp.line);
        String thisLine = Texts.trim(doc.getText(TextRange.create(thisStart, thisEnd)));
        char sep;
        if(thisLine.startsWith("|")){
            sep = '|';
        }else{
            sep = ',';
        }
//        //是否进入markdown表格编辑模式
//        if (!isMarkDownTableLine(thisLine)) {
//            return;
//        }

        //把插件无关的代码放到其他类，这样可以写单元测试，否则会找不到类（比如AnAction）
        //查找當前行之前的属于markdown表格的行
        List<String> prevLines = findTablePrevLines(doc, vp.line, sep);
        //查找當前行之后的属于markdown表格的行
        List<String> nextLines = findTableNextLines(doc, vp.line, doc.getLineCount(), sep);

        //表格的所有行
        List<String> origLines = new ArrayList<>();
        origLines.addAll(prevLines);
        origLines.add(thisLine);
        origLines.addAll(nextLines);

        //收集表格数据
        List<List<String>> tableData = Texts.toTable(origLines,sep);
        List<String> newlines;
        if (actionName.equals("Align")) {
            //计算每列的宽
            List<Integer> columnWidths = Texts.calcColumnWidth(tableData);

            //根据列宽和表格数据，生成新的表格
            newlines = Texts.alignTable(tableData, columnWidths, sep);
        } else if (actionName.equals("Trim")) {
            newlines = Texts.trimTable(tableData, sep);
        } else {
            throw new RuntimeException("action " + actionName + " is not supported!");
        }

        //替换原有表格
        //int end = primaryCaret.getSelectionEnd();
        int start = doc.getLineStartOffset(vp.line - prevLines.size());
        int end = doc.getLineEndOffset(vp.line + nextLines.size());
        // Replace the selection with a fixed string.
        // Must do this doc change in a write action context.
        WriteCommandAction.runWriteCommandAction(project, () ->
                doc.replaceString(start, end, String.join("\n", newlines))
        );

        // De-select the text range that was just replaced
        primaryCaret.removeSelection();
    }

    private static List<String> findTableNextLines(Document doc, int lineNum, int lineCount, char sep) {
        int lastLineNum = lineNum;
        List<String> lines = new ArrayList<>();
        while (lastLineNum < lineCount - 1) {
            int nextStart = doc.getLineStartOffset(lastLineNum + 1);
            int nextEnd = doc.getLineEndOffset(lastLineNum + 1);
            String nextLine = doc.getText(TextRange.from(nextStart, nextEnd - nextStart));
            if (isTableLine(nextLine, sep)) {
                lines.add(nextLine);
                lastLineNum++;
            } else {
                break;
            }
        }
        return lines;
    }

    private static List<String> findTablePrevLines(Document doc, int lineNum, char sep) {
        LinkedList<String> lines = new LinkedList<>();
        int firstLineNum = lineNum;
        //行号从0开始
        while (firstLineNum > 0) {
            int prevStart = doc.getLineStartOffset(firstLineNum - 1);
            int prevEnd = doc.getLineEndOffset(firstLineNum - 1);
            String prevLine = doc.getText(TextRange.from(prevStart, prevEnd - prevStart));
            if (isTableLine(prevLine, sep)) {
                lines.addFirst(prevLine);
                firstLineNum--;
            } else {
                break;
            }
        }
        return lines;
    }

    private static boolean isTableLine(String line, char sep) {
        return line.indexOf(sep)>-1;
    }
}
