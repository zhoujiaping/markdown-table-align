package org.jp.markdown;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.jetbrains.annotations.NotNull;

public class MarkdownTableAlignAction extends AnAction {

    //    @Override
//    public void update(@NotNull AnActionEvent event) {
//        // Get required data keys
//        Project project = event.getProject();
//        Editor editor = event.getData(CommonDataKeys.EDITOR);
//        // ...
//        // Set visibility only in the case of
//        // existing project editor, and selection
//        event.getPresentation().setEnabledAndVisible(project != null
//                && editor != null && editor.getSelectionModel().hasSelection());
//    }
    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        ActionPerformer.perform(event,"Align");
    }
}
