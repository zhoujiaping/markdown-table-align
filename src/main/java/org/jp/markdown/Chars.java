package org.jp.markdown;

public class Chars {
    public static final char chineseWhitespace = '　';
    public static final char asciiWhitespace = ' ';
    /**
     * 根据Unicode编码完美的判断中文汉字和符号
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }
    public static int chineseCharCount(CharSequence seq){
        if(seq==null){
            return 0;
        }
        int len = seq.length();
        int chineseCharCount = 0;
        for(int i=0;i<len;i++){
            if(Chars.isChinese(seq.charAt(i))){
                chineseCharCount++;
            }
        }
       return chineseCharCount;
    }

    public static void main(String[] args) {
        System.out.println(isChinese('　'));
        System.out.println(isChinese(' '));
    }
}
