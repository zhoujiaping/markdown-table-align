package org.jp.markdown;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Texts {
    public static int stringWidthWithPaddingChinese(String str) {
        int chineseCharCount = Chars.chineseCharCount(str);
        return (str.length() - chineseCharCount) + (int) Math.ceil(chineseCharCount / 5.0) * 8;
    }

    public static List<List<String>> toTable(List<String> origLines, char sep) {
        List<List<String>> tableData = new ArrayList<>();
        for (String row : origLines) {
            List<String> rowData = new ArrayList<>();
            tableData.add(rowData);
            List<String> cols = split(row, sep);
            for (int i = 0; i < cols.size(); i++) {
                rowData.add(Texts.trim(cols.get(i)));
            }
        }
        return tableData;
    }

    public static List<String> split(String str, char sep) {
        if (str == null) {
            return List.of();
        }
        char[] chs = str.toCharArray();
        List<String> parts = new ArrayList<>();
        StringBuilder part = new StringBuilder();
        for (int i = 0; i < chs.length; i++) {
            if (chs[i] == '\\') {
                part.append(chs[i++]);
                if (i < chs.length) {
                    part.append(chs[i]);
                }
            } else if (chs[i] == sep) {
                parts.add(part.toString());
                part = new StringBuilder();
            } else {
                part.append(chs[i]);
            }
        }
        parts.add(part.toString());
        return parts;
    }

    public static String padRight(String s, int maxLen) {
        StringBuilder sb = new StringBuilder(s);

        int chineseCharCount = Chars.chineseCharCount(s);
        int remainChineseCount = chineseCharCount % 5;
        int[] repeatTimes = {0, 4, 3, 2, 1};
        sb.append(repeat(Chars.chineseWhitespace, repeatTimes[remainChineseCount]));
        int widthPaddedChinese = stringWidthWithPaddingChinese(s);
        sb.append(repeat(Chars.asciiWhitespace, maxLen - widthPaddedChinese));
        return sb.toString();
    }

    public static String repeat(char ch, int times) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < times; i++) {
            sb.append(ch);
        }
        return sb.toString();
    }

    /**
     * 对齐：中文5个字符对8个英文字符
     * |一二三四五|
     * |12345678|
     */
    public static List<Integer> calcColumnWidth(List<List<String>> tableData) {

        List<Integer> widths = new ArrayList<>();
        for (List<String> rowData : tableData) {
            for (int i = 0; i < rowData.size(); i++) {
                if (widths.size() <= i) {
                    widths.add(0);
                }
                String colData = rowData.get(i);
                int w = stringWidthWithPaddingChinese(colData);
                if (widths.get(i) < w) {
                    widths.set(i, w);
                }
            }
        }
        return widths;
    }

    public static List<String> alignTable(List<List<String>> tableData, List<Integer> columnWidths, char sep) {
        String _sep = sep+"";
        List<String> newlines = new ArrayList<>();
        for (List<String> rowData : tableData) {
            List<String> newCols = new ArrayList<>();
            for (int i = 0; i < rowData.size(); i++) {
                newCols.add(" " + padRight(rowData.get(i), columnWidths.get(i)) + " ");
            }
            //换行符不能用\r\n,只能用\n。不然被认为是不合法的换行符而报错
            newlines.add(String.join(_sep, newCols));
        }
        return newlines;
    }

    public static List<String> trimTable(List<List<String>> tableData, char sep) {
        String _sep = sep+"";
        List<String> newlines = new ArrayList<>();
        for (List<String> rowData : tableData) {
            List<String> newCols = new ArrayList<>();
            for (int i = 0; i < rowData.size(); i++) {
                newCols.add(trim(rowData.get(i)));
            }
            //换行符不能用\r\n,只能用\n。不然被认为是不合法的换行符而报错
            newlines.add(String.join(_sep, newCols));
        }
        return newlines;
    }

    /**
     * String#trim不支持trim掉中文空白字符，所以实现一个可以trim掉中文空白字符的版本
     */
    public static String trim(String str) {
        if (str == null) {
            return null;
        }
        if (str.equals("")) {
            return "";
        }
        char[] chs = str.toCharArray();
        int i = 0;
        for (; i < chs.length; i++) {
            if (chs[i] != Chars.chineseWhitespace && !Character.isWhitespace(chs[i])) {
                break;
            }
        }
        int j = chs.length - 1;
        for (; j >= 0 && j > i; j--) {
            if (chs[j] != Chars.chineseWhitespace && !Character.isWhitespace(chs[j])) {
                break;
            }
        }
        if(i<=j){
            return new String(chs, i, j-i+1);
        }else{
            return "";
        }
    }
    public static String trimLeft(String str) {
        if (str == null) {
            return null;
        }
        if (str.equals("")) {
            return "";
        }
        char[] chs = str.toCharArray();
        int i = 0;
        for (; i < chs.length; i++) {
            if (chs[i] != Chars.chineseWhitespace && !Character.isWhitespace(chs[i])) {
                break;
            }
        }
        if(i<chs.length){
            return new String(chs, i, chs.length);
        }else{
            return "";
        }
    }
    public static String trimRight(String str) {
        if (str == null) {
            return null;
        }
        if (str.equals("")) {
            return "";
        }
        char[] chs = str.toCharArray();
        int j = chs.length - 1;
        for (; j >= 0; j--) {
            if (chs[j] != Chars.chineseWhitespace && !Character.isWhitespace(chs[j])) {
                break;
            }
        }
        if(0<=j){
            return new String(chs, 0, j+1);
        }else{
            return "";
        }
    }

    public static void main(String[] args) {
        String s = "| 这是第一列 | c2                              |\n" +
                "| ---      | ---                             |\n" +
                "| hello    | https://www.baidu.com/index.php |\n" +
                "|   哈哈哈 |                     1               |\n" +
                "| x        | y                               |\n" +
                "| 哈　　　　 | https://www.baidu.com/index.php |\n" +
                "| 哈哈　　　 | https://www.baidu.com/index.php |\n" +
                "| 哈哈哈　　 | https://www.baidu.com/index.php |\n" +
                "| 哈哈哈哈　 | https://www.baidu.com/index.php |\n" +
                "| 哈哈哈哈哈 |                                 |";
        var sep = '|';
        var tableData = toTable(s.lines().collect(Collectors.toList()), sep);
        var columnWidths = calcColumnWidth(tableData);
        System.out.println(columnWidths);
        List<String> newlines = Texts.alignTable(tableData, columnWidths, sep);
        System.out.println(String.join("\n",newlines));
        System.out.println(trim(""));
        System.out.println(trim(" "));
        System.out.println(trim("   "));
        System.out.println(trim("1   "));
        System.out.println(trim("1   1"));
        System.out.println(trim("  1   "));
        System.out.println(trim("  1   "));
    }

}
