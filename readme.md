# introduce

Align Markdown table(in source file, align columns separator). when the
Caret is in Markdown table, press shortcut key(Alt+Caps Lock) to align
Markdown table.

example

before align：

```
|col1|col2|col3|col4|
|---|---|---|---|
|clojure|scala|groovy|c|
|it is funny!|good|js in jvm||
```

after align：

```
| col1         | col2  | col3      | col4 |
| ---          | ---   | ---       | ---  |
| clojure      | scala | groovy    | c    |
| it is funny! | good  | js in jvm |      |
```

it supports chinese characters in table.

Markdown表格对齐（源文件中，对齐列分隔符） 当光标在Markdown表格里面的时候，
按快捷键（Alt+Caps Lock）自动对齐Markdown表格。

表格中有中文内容也可以对齐。

# problems

1. 无法安装，idea版本不匹配。
   用压缩软件打开插件（jar包），修改META-INF/plugin.xml。

```
<idea-version since-build="212.5284" until-build="231.*" />
```

2. 菜单位置 Tools/Markdown Table/Align
3. 快捷键配置 如果快捷键Alt+Caps Lock没生效，则需要自己设置一下快捷键
4. 该插件实现对齐的原理是在文本后面加中文空格和英文空格。  
   对齐后，markdown如果设置了左对齐、右对齐或者居中，那么markdown显示的时候就多出来空格，从而在渲染结果上无法对齐。  
   解决办法是编辑完表格后，按Alt+Shift+Caps Lock将表格trim一下。  
   当需要编辑表格时，按Alt+Caps Lock对齐后再编辑。  
   同样，如果快捷键没有生效，需要手动设置一下快捷键。

